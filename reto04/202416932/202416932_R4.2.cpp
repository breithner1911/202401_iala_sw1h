#include <iostream>

using namespace std;

int** crearMatriz(int n, int m) {
    int** matriz = new int* [n];
    for (int i = 0; i < n; ++i) {
        matriz[i] = new int[m]();
    }
    return matriz;
}

void liberarMatriz(int** matriz, int n) {
    for (int i = 0; i < n; ++i) {
        delete[] matriz[i];
    }
    delete[] matriz;
}

void imprimirMatriz(int** matriz, int n, int m) {
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < m; ++j) {
            cout << matriz[i][j] << " ";
        }
        cout << endl;
    }
}

int main() {
    int n, m;
    cout << "Ingrese el numero de filas de la matriz: ";cin >> n;
    cout << "Ingrese el numero de columnas de la matriz: ";cin >> m;

    int** matriz = crearMatriz(n, m);

    cout << "\nResultado de la matriz:" << endl;
    imprimirMatriz(matriz, n, m);

    liberarMatriz(matriz, n);

    system("pause");
    return 0;
}
