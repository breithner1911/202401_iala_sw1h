#include <iostream>


using namespace std;

int** newmatriz(int x, int y) {
    int** matriz = new int* [x];
    for (int i = 0; i < x; i++) {
        matriz[i] = new int[y];
        for (int j = 0; j < y; j++) {
            matriz[i][j] = 0;
        }
    }
    return matriz;
}

void imprimirmatriz(int** matriz, int x, int y) {
    for (int i = 0; i < x; i++) {
        for (int j = 0; j < y; j++) {
            cout << matriz[i][j] << " ";
        }
        cout << endl;
    }
}

int main() {
    int x, y;
    cout << "Ingrese n: ";
    cin >> x;
    cout << "Ingrese m: ";
    cin >> y;

    int** matriz = newmatriz(x, y);

    cout << "La matriz " << x << "x" << y << " es: " << endl;
    imprimirmatriz(matriz, x, y);

    for (int i = 0; i < x; i++) {
        delete[] matriz[i];
    }
    delete[] matriz;
    system("pause");
    return 0;
}
